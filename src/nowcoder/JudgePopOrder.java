package nowcoder;

import java.util.ArrayList;
import java.util.List;

public class JudgePopOrder {

	public static void main(String[] args) {
		int[] a = {1,2,3,4,5};
		int[] b = {4,5,3,2,1};
		int[] c = {4,3,5,1,2};
		System.out.println(isPopOrder(a,b));
		System.out.println(isPopOrder(a,c));
	}
	
	public static boolean isPopOrder(int[] pushA, int[] popA) {
		List<Integer> push = new ArrayList<>();
		List<Integer> pop = new ArrayList<>();
		for(int i = 0; i < popA.length; i++) {
			push.add(pushA[i]);
			pop.add(popA[i]);
		}
		List<Integer> visited = new ArrayList<>();
		
		int current,index,previousIndex,currentIndex;
	    //对pop里的元素逐一遍历，判断
		for(int i = 0; i < popA.length; i++) {
			current = popA[i];
			//在pushA中，位于current之前的元素,在popA当中需要反序
			index = push.indexOf(current);
			if(index == -1)
				return false;
			previousIndex = i;
			for(int j = index - 1; j >= 0; j--) {
				if(!visited.contains(push.get(j))) {
					currentIndex = pop.indexOf(push.get(j));
					if(currentIndex < previousIndex)
						return false;
					previousIndex = currentIndex;
				}
			}
			visited.add(current);
		}
		
		return true;
    }

}
