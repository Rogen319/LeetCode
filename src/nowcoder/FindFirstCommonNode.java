package nowcoder;

public class FindFirstCommonNode {

	public static void main(String[] args) {

	}
	
	//暴力法求解：双层循环
	public ListNode findFirstCommonNodeNaive(ListNode pHead1, ListNode pHead2) {
		 ListNode current1 = pHead1;
		 ListNode current2;
		 
		 while(current1 != null) {
			 current2 = pHead2;
			 while(current2 != null) {
				 if(current1.val == current2.val)
					 return current1;
				 current2 = current2.next;
			 }
			 current1 = current1.next;
		 }
		 
		 return null;
    }
	
	//通过两个指针记录两个链表的位置
	public ListNode improvedFindFirstCommonNodeNaive(ListNode pHead1, ListNode pHead2) {
		int l1 = getLength(pHead1);
		int l2 = getLength(pHead2);
		
		ListNode longList, shortList;
		int steps;
		
		if(l1 >= l2) {
			longList = pHead1;
			shortList = pHead2;
			steps = l1 - l2;
		}else {
			longList = pHead2;
			shortList = pHead1;
			steps = l2 - l1;
		}
		
		for(int i = 0; i < steps; i++) {
			longList = longList.next;
		}
		
		while(longList != null && shortList!= null && longList != shortList) {
			longList = longList.next;
			shortList = shortList.next;
		}
		
		return longList;
	}

	private int getLength(ListNode pHead) {
		int res = 0;
		while(pHead != null) {
			res++;
			pHead = pHead.next;
		}
		return res;
	}

}

class ListNode{
	int val;
	ListNode next = null;
	
	ListNode(int val){
		this.val = val;
	}
}
