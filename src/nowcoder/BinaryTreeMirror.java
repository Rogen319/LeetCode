package nowcoder;

import java.util.LinkedList;

public class BinaryTreeMirror {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public void Mirror(TreeNode root) {
		if(root == null)
			return;
		
		LinkedList<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		TreeNode current;
		TreeNode swap;
		while(!queue.isEmpty()) {
			current = queue.poll();
			swap = current.left;
			current.left = current.right;
			current.right = swap;
			if(current.left != null)
				queue.add(current.left);
			if(current.right != null)
				queue.add(current.right);
		}
	}

}
