package nowcoder;

public class FindKthToTail {

	public static void main(String[] args) {
		

	}
	
	public ListNode findKthToTail(ListNode head,int k) {
		
		if(head == null || k <= 0)
			return null;
		
		ListNode p1 = head, p2 = head;
		
		for(int i = 1; i < k; i++) {
			p2 = p2.next;
		}
        
        if(p2 == null)
            return null;
		
		while(p2.next != null) {
			p1 = p1.next;
			p2 = p2.next;
		}
		
		return p1;
    }
	

}
