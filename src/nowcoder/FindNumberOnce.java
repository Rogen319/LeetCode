package nowcoder;

import java.util.HashMap;
import java.util.Map;

public class FindNumberOnce {

	public void FindNumsAppearOnce(int[] array,int num1[] , int num2[]) {
		if(array == null || array.length == 0)
			return;
		
		Map<Integer, Integer> map = new HashMap<>();
		
		for(int i = 0; i < array.length; i++) {
			if(map.get(array[i]) != null) {
				map.put(array[i], map.get(array[i]) + 1);
			}else {
				map.put(array[i], 1);
			}
		}
		
		int index = 0;
		for(Map.Entry<Integer, Integer> entry : map.entrySet()) {
			if(entry.getValue() == 1) {
				index++;
				if(index == 1)
					num1[0] = entry.getKey();
				else if(index == 2)
					num2[0] = entry.getKey();
			}
		}
	}

}
