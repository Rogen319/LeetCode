package nowcoder;

import java.util.Stack;

public class StackWithMin {
	
	Stack<Integer> data = new Stack<>();
	Stack<Integer> min = new Stack<>();

	public void push(int node) {
		data.push(node);
		//很巧妙，如果当前push的值最小，也直接入栈；否则，在min的栈里，push之前的最小值
		if(min.isEmpty() || node < min.peek())
			min.push(node);
		else
			min.push(min.peek());
	}

	public void pop() {
		data.pop();
		min.pop();
	}

	public int top() {
		if(!data.isEmpty())
			return data.peek();
		return -1;
	}

	public int min() {
		if(!min.isEmpty())
			return min.peek();
		return -1;
	}
}
