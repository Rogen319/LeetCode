package nowcoder;

import java.util.ArrayList;

public class FindLeastKNumber2 {

	public static void main(String[] args) {
		int[] a = {4,5,1,6,2,7,3,8};
		System.out.println(GetLeastNumbers_Solution(a, 1));
	}
	
	public static ArrayList<Integer> GetLeastNumbers_Solution(int[] input, int k) {
		ArrayList<Integer> res = new ArrayList<>();
		if(k <= 0 || input == null || input.length == 0 || k > input.length)
			return res;
		
		//���ÿ���
		int l = 0, r = input.length - 1;
		int index = partition(input, l ,r);
		
		while(index != k - 1) {
			if(index > k - 1){
                r = index - 1;
                index = partition(input, l, r);
            }
				
			else{
                l = index + 1;
				index = partition(input, l, r);
            }
		}
		
		for(int i = 0; i < k; i++) {
			res.add(input[i]);
		}
		
		return res;
	}

	private static int partition(int[] input, int l, int r) {
		int pivotPosition = (int)(l + Math.random() * (r - l + 1));
		int pivot = input[pivotPosition];
		
		swap(input, pivotPosition, r);
		int smallIndex = l - 1;
		for(int i = l; i <= r; i++) {
			if(input[i] <= pivot) {
				smallIndex++;
				if(i > smallIndex)
					swap(input, smallIndex, i);
			}
		}
		return smallIndex;
	}

	private static void swap(int[] input, int index1, int index2) {
		int temp = input[index1];
		input[index1] = input[index2];
		input[index2] = temp;
	}

}
