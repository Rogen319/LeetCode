package nowcoder;

import java.util.ArrayList;

public class PrintMatrix {
	
	public static void main(String[] args) {
		int[][] matrix = {{1,2},{3,4},{5,6},{7,8},{9,10}};
		System.out.println(printMatrix(matrix));
	}

	public static ArrayList<Integer> printMatrix(int [][] matrix) {
		ArrayList<Integer> res = new ArrayList<>();
		
		if(matrix == null || matrix.length == 0)
			return res;
		
		if(matrix.length == 1) {
			for(int i = 0; i < matrix[0].length; i++)
				res.add(matrix[0][i]);
			return res;
		}
		
		if(matrix[0].length == 1) {
			for(int i = 0; i < matrix.length; i++)
				res.add(matrix[i][0]);
			return res;
		}

		int rows = matrix.length;
		int columns = matrix[0].length;
		double rd = rows;
		double cd = columns;
		int times = (int) Math.min(Math.ceil(rd / 2), Math.ceil(cd / 2));
		
		for(int i = 0; i < times; i++) {
			int firstRow = i, lastRow = rows - 1 - i;
			int firstColumn = columns - 1 - i, lastColumn = i;
			if(firstRow == lastRow) {
				//第一行
				for(int j = i; j <= firstColumn; j++) 
					res.add(matrix[firstRow][j]);
			}else {
				//第一行
				for(int j = i; j <= firstColumn; j++) 
					res.add(matrix[firstRow][j]);
				//第一列
				for(int j = firstRow + 1; j < lastRow; j++)
					res.add(matrix[j][firstColumn]);
				//最后一行
				for(int j = firstColumn; j >= i; j--)
					res.add(matrix[lastRow][j]);
				//最后一列
				for(int j = lastRow - 1; j > firstRow; j--)
					res.add(matrix[j][lastColumn]);
			}
			
		}

		return res;
	}

}
