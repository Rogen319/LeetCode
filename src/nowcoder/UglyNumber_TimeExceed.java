package nowcoder;

public class UglyNumber_TimeExceed {

	public static void main(String[] args) {
		System.out.println(GetUglyNumber_Solution(10));
	}
	
	public static int GetUglyNumber_Solution(int index) {
		int count = 1;
		int number = 1;
		boolean flag;
		
		int t;
		while(count < index) {
			number++;
			flag = true;
			for(int i = 2; i <= Math.sqrt(number); i++) {
				if(number % i == 0) {
					t = number / i;
					if(!isValid(i) || !isValid(t)) {
						flag = false;
						break;
					}
					
				}
			}
			if(!isValid(number))
				flag = false;
			if(flag)
				count++;
		}
		
        return number;
    }

	private static boolean isValid(int number) {
		if(isPrime(number) && number != 2 && number != 3 && number != 5 )
			return false;
		return true;
	}

	private static boolean isPrime(int number) {
		for(int i = 2; i <= Math.sqrt(number); i++) {
			if(number % i == 0)
				return false;
		}
		return true;
	}

}
