package nowcoder;

import java.util.ArrayList;
import java.util.LinkedList;

public class ZhiBinaryTree {

	public static void main(String[] args) {

	}

	public ArrayList<ArrayList<Integer> > print(TreeNode pRoot) {
		ArrayList<ArrayList<Integer>> res = new ArrayList<>();
		
		if(pRoot == null)
			return res;

		LinkedList<TreeNode> queue = new LinkedList<>();
		queue.add(pRoot);

		int length;
		int flag = 1;//1代表从左到右，-1代表从右到左
		TreeNode temp;
		while(!queue.isEmpty()) {
			length = queue.size();
			ArrayList<Integer> single = new ArrayList<>();

			while(length > 0) {
				temp = queue.pollFirst();
				single.add(temp.val);
				if(temp.left != null)
					queue.add(temp.left);
				if(temp.right != null)
					queue.add(temp.right);
				length--;
			}
			//从左到右
			if(flag > 0) {
				res.add(single);
			}
			//从右到左
			else {
				ArrayList<Integer> tempList = new ArrayList<>();
				for(int i = single.size() - 1; i >= 0; i--) {
					tempList.add(single.get(i));
				}
				res.add(tempList);
			}
			flag = -flag;
			
		}

		return res;
	}

}

class TreeNode {
	int val = 0;
	TreeNode left = null;
	TreeNode right = null;

	public TreeNode(int val) {
		this.val = val;

	}

}
