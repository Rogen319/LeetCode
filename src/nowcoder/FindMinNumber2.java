package nowcoder;

import java.util.Arrays;

public class FindMinNumber2 {
	
	public static void main(String... args) {
		int[] a = {3,32,321};
		System.out.println(PrintMinNumber(a));
	}
	
	public static String PrintMinNumber(int[] numbers) {
		String[] numbers_string = new String[numbers.length];
		for(int i = 0; i < numbers.length; i++) {
			numbers_string[i] = String.valueOf(numbers[i]);
		}
		
		Arrays.sort(numbers_string, (s1, s2)->{
			String temp1 = s1 + s2;
			String temp2 = s2 + s1;
			return temp1.compareTo(temp2);
		});
		
		StringBuilder sb = new StringBuilder();
		Arrays.stream(numbers_string).forEach(s->sb.append(s));
		
		return sb.toString();
    }

}
