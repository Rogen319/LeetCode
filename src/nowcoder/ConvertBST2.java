package nowcoder;

public class ConvertBST2 {

	public TreeNode Convert(TreeNode pRootOfTree) {
		if(pRootOfTree == null)
			return null;

		//递归左子树
		if(pRootOfTree.left != null) {
			//叶子节点
			if(pRootOfTree.left.left == null && pRootOfTree.left.right == null) {
				pRootOfTree.left.right = pRootOfTree;
			}else {
				Convert(pRootOfTree.left);
				//改变右节点的指针
				if(pRootOfTree.left.right != null)
					pRootOfTree.left = pRootOfTree.left.right;
				pRootOfTree.left.right = pRootOfTree;
			}
		}

		//递归右子树
		if(pRootOfTree.right != null) {
			//叶子节点
			if(pRootOfTree.right.left == null && pRootOfTree.right.right == null) {
				pRootOfTree.right.left = pRootOfTree;
			}else {
				Convert(pRootOfTree.right);
				if(pRootOfTree.right.left != null)
					pRootOfTree.right = pRootOfTree.right.left;
				pRootOfTree.right.left = pRootOfTree;
			}
		}

		return findLeft(pRootOfTree);
	}

	private TreeNode findLeft(TreeNode pRootOfTree) {
		while(pRootOfTree.left != null)
			pRootOfTree = pRootOfTree.left;

		return pRootOfTree;
	}

}
