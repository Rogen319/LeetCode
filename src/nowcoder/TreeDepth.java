package nowcoder;

import java.util.ArrayList;
import java.util.List;

public class TreeDepth {

	public int TreeDepth2(TreeNode root) {
		if(root == null)
			return 0;
		List<Integer> depths = new ArrayList<>();
		getAllDepths(root, depths, 0);
		return depths.stream().max((i1, i2)-> i1 - i2).get();
	}

	private void getAllDepths(TreeNode node, List<Integer> depths, int depth) {
		depth++;
		if(node.left == null && node.right == null)
			depths.add(depth);
		if(node.left != null)
			getAllDepths(node.left, depths, depth);
		if(node.right != null)
			getAllDepths(node.right, depths, depth);
	}
	
}
