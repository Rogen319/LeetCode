package nowcoder;

public class JudgeBalanced {

	public boolean IsBalanced_Solution(TreeNode root) {
		if(root == null)
			return true;
		int leftDep = getDepth(root.left);
		int rightDep = getDepth(root.right);
		if(Math.abs(leftDep - rightDep) > 1)
			return false;
		return IsBalanced_Solution(root.left) && IsBalanced_Solution(root.right);
	}
	
	public int getDepth(TreeNode node) {
		if(node == null)
			return 0;
		int leftDep = 0, rightDep = 0;
		if(node.left != null)
			leftDep = getDepth(node.left) + 1;
		if(node.right != null)
			rightDep = getDepth(node.right) + 1;
		return (leftDep > rightDep) ? leftDep : rightDep;
	}

}
