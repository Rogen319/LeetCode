package nowcoder;

public class CloneList {

	public RandomListNode clone(RandomListNode pHead){
        if(pHead == null)
        	return null;
        RandomListNode newHead = new RandomListNode(pHead.label);
//        newHead.random = pHead.random;//random这里指向了原链表中的元素，错误，应该指向新链表中的元素
        RandomListNode current = newHead, p = pHead;
        
        while(p.next != null) {
        	RandomListNode next = new RandomListNode(p.next.label);
        	current.next = next;
        	current = current.next;
        	p = p.next;
        }
        
        //设定random
        current = newHead;
        p = pHead;
        RandomListNode originP, newP;
        while(p != null) {
        	originP = pHead;
        	newP = newHead;
        	while(originP != p.random) {
        		originP = originP.next;
        		newP = newP.next;
        	}
        	current.random = newP;
        	current = current.next;
        	p = p.next;
        }
        
        return newHead;
    }
}

class RandomListNode {
    int label;
    RandomListNode next = null;
    RandomListNode random = null;

    RandomListNode(int label) {
        this.label = label;
    }
}
