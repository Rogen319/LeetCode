package nowcoder;

public class UglyNumber_TimeExceed2 {
	
	public static void main(String[] args) {
		System.out.println(GetUglyNumber_Solution(10));
	}
	
	public static int GetUglyNumber_Solution(int index) {
		int count = 1;
		int number = 1;
		while(count < index) {
			number++;
			if(isUglyNumber(number))
				count++;
		}
		return number;
	}

	private static boolean isUglyNumber(int number) {
		while(number % 2 == 0)
			number = number / 2;
		while(number % 3 == 0)
			number = number / 3;
		while(number % 5 == 0)
			number = number / 5;
		if(number == 1)
			return true;
		return false;
	}

}
