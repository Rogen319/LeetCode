package nowcoder;

public class FirstNotRepeatingChar {
	
	public int firstNotRepeatingChar(String str) {
		char c;
		for(int i = 0; i < str.length(); i++) {
			c = str.charAt(i);
			if(str.indexOf(c) == str.lastIndexOf(c))
				return i;
		}
        return -1;
    }

}
