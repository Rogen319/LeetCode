package nowcoder;

public class GetNumberOfK {
	
	public static void main(String[] args) {
		int[] a = {1,3,3,3,3,4,5};
		System.out.println(GetNumberOfK2(a,0));
	}

	public static int GetNumberOfK2(int[] array , int k) {
		int first = -1, last = -1;
		int l = 0;
		int r = array.length - 1;
		int mid = l + ((r - l)>>1);;
		//找到第一个位置
		while(l >= 0 && r < array.length && l <= r) {
			if(array[mid] > k)
				r = mid - 1;
			else if(array[mid] < k)
				l = mid + 1;
			else {
				if(mid == 0 || (mid - 1 >= 0 && array[mid - 1] != k)) {
					first = mid;
					break;
				}else {
					r = mid - 1;
				}
			}
			mid = (l + r) / 2;
		}
		//找到最后一个位置
		l = first;
		r = array.length - 1;
		mid = l + ((r - l)>>1);
		while(l >= 0 && r < array.length && l <= r) {
			System.out.println(mid + "\t" + array[mid]);
			if(array[mid] > k)
				r = mid - 1;
			else if(array[mid] < k)
				l = mid + 1;
			else {
				if(mid == array.length - 1 || (mid + 1 < array.length && array[mid + 1] != k)) {
					last = mid;
					break;
				}else {
					l = mid + 1;
				}
			}
			mid = (l + r) / 2;
		}
		if(first != -1 && last != -1)
			return last - first + 1;
		
		return 0;
		
//		int count = 1;
//		int index = mid - 1;
//		while(index >= 0 && array[index] == k) {
//			count++;
//			index--;
//		}
//		
//		index = mid + 1;
//		while(index < array.length && array[index] == k) {
//			count++;
//			index++;
//		}
//		
//		return count;
	}

}
