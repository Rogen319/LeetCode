package nowcoder;

import java.util.ArrayList;

public class FindLeastKNumber {
	
	public static void main(String[] args) {
		int[] a = {4,5,1,6,2,7,3,8};
		System.out.println(GetLeastNumbers_Solution(a, 4));
	}

	public static ArrayList<Integer> GetLeastNumbers_Solution(int[] input, int k) {
		ArrayList<Integer> res = new ArrayList<>();
		if(input == null || input.length == 0 || k > input.length)
			return res;
		
		makeHeap(input);
		int l = input.length;
		
		for(int i = 0; i < k; i++) {
			swap(input, 0, l - 1 - i);
			res.add(input[l - 1 - i]);
			adjustDown(input, 0, l - 2 - i);
		}
		return res;
	}
	
	//建堆
	private static void makeHeap(int[] array) {
		int l = array.length;
		
		//从第一个非叶子结点开始遍历
		for(int i = l/2 - 1; i >= 0; i--) {
			adjustDown(array, i, l);
		}
	}

	//调整
	private static void adjustDown(int[] array, int i, int l) {
		int child = 2 * i + 1;
		while(child < l) {
			//找到左右子节点中较小的
			if(child + 1 < l && array[child] > array[child + 1])
				child++;
			//父节点已是最小，直接break
			if(array[i] < array[child])
				break;
			swap(array, i, child);
			//调整以后，看是否会影响到底层的数据
			i = child;
			child = 2 * i + 1;
		}
	}

	private static void swap(int[] array, int index1, int index2) {
		int temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
	}

}
