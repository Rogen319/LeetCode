package nowcoder;

public class ReOrderArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	//调整数组元素顺序，使得奇数在前，偶数在后
	public void reOrderArray(int[] array) {
		if(array == null || array.length == 0)
			return;
		int begin = 0, from;
		while(begin < array.length) {
			while(begin < array.length && (array[begin] % 2 == 1))
				begin++;
			from = begin + 1;
			while(from < array.length && (array[from] % 2 == 0))
				from++;
			if(from < array.length) {
				int temp = array[from];
				for(int i = from - 1; i >= begin; i--) {
					array[i + 1] = array[i];
				}
				array[begin] = temp;
			}else {
				break;
			}
		}
	}

}
