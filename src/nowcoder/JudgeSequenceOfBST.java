package nowcoder;

import java.util.ArrayList;
import java.util.List;

public class JudgeSequenceOfBST {
	
	public static void main(String[] args) {
		int[] a = {2,3,5,4,7,12,10,8,6};
		System.out.println(verifySquenceOfBST(a));
	}
	
	public static boolean verifySquenceOfBST(int[] sequence) {
		if(sequence == null || sequence.length == 0)
			return false;
		List<Integer> list = new ArrayList<>();
		for (Integer integer : sequence) {
			list.add(integer);
		}
		return judgeSequence(list);
	}

	private static boolean judgeSequence(List<Integer> list) {
		//list的最后一个元素为根节点，只需判断是否能够将list划分为左右两个子串即可，左子串都小于根，右子串都大于根
		if(list.size() == 0 || list.size() == 1)
			return true;
		
		int root = list.get(list.size() - 1);
		List<Integer> left = new ArrayList<>();
		List<Integer> right = new ArrayList<>();
		int index = 0;
		while (list.get(index) < root && index < list.size()) {
			left.add(list.get(index));
			index++;
		}
		for(; index < list.size() - 1; index++) {
			if(list.get(index) > root) {
				right.add(list.get(index));
			}else
				return false;
		}
		return judgeSequence(left) && judgeSequence(right);
	}
}
