package fail;

public class RegularExpMatching10_NotPassed {

	public static void main(String[] args) {
		System.out.println(isMatch("aa","a"));
		System.out.println(isMatch("aa","aa"));
		System.out.println(isMatch("aaa","aa"));
		System.out.println(isMatch("aa", "a*"));
		System.out.println(isMatch("aa", ".*"));
		System.out.println(isMatch("ab", ".*"));
		System.out.println(isMatch("aab", "c*a*b"));
		System.out.println(isMatch("ab", ".*c"));
		System.out.println(isMatch("aa", "a*"));
	}

	public static boolean isMatch(String s, String p) {
		boolean result = true;
		
		//指针：记录s和p当前的匹配位置
		int i = 0, j = 0;
		while(i < s.length() && j < p.length()) {
			char c1 = s.charAt(i);
			char c2 = p.charAt(j);
			if(c2 == '.' || c1 == c2) {
				i++;
				j++;
				continue;
			}
			if(c1 != c2) {
				//a和c*情况
				if(j + 1 < p.length() && p.charAt(j + 1) == '*') {
					j += 2;
					continue;
				}
				/*
				 * .*和*应该匹配多少呢？
				 * 比如，(aaa,a*a)其实只能匹配一个，否则会输出false
				 */
				//aa和a*情况
				if(c2 == '*' && i - 1 >= 0 && j - 1 >= 0) {
					if(p.charAt(j - 1) == '.')
						i++;
					else if(c1 == s.charAt(i - 1) && s.charAt(i - 1) == p.charAt(j - 1))
						i++;
					else
						j++;
					continue;
				}
				result = false;
				break;
			}
			
		}
		if(i != s.length() || j != p.length()) {
			if(j == p.length() - 1 && p.charAt(j) == '*') {
				
			}else {
				result = false;
			}
		}
		
		return result;
	}

}
