package fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindSubstring30_TimeLimitExceed {

	public static void main(String[] args) {
		String[] words = {"word","good","best","good"};
		System.out.println(findSubstring("wordgoodgoodgoodbestword", words));
	}

	public static List<Integer> findSubstring(String s, String[] words) {
		List<Integer> result = new ArrayList<>();
		if(words.length == 0)
			return result;
		//记录words里面的每个word以及出现的次数
		Map<String, Integer> map;
		//取出第一个words并计算单个Word的长度以及总长度
		String begin = words[0];
		int length = begin.length();
		int totalLength = words.length * length;
		//记录是否需要重新初始化map
		boolean isChanged = false;
		
		//初始化map
		map = initMap(words);
		
		for(int i = 0; i <= s.length() - totalLength; i++) {
			if(isChanged)
				map = initMap(words);
			String single = s.substring(i, i + totalLength);
			System.out.println(single);
			while(single.length() > 0) {
				String word = single.substring(0, length);
				System.out.println(word);
				if(map.get(word) != null && map.get(word) > 0) {
					map.put(word, map.get(word) - 1);
					single = single.substring(length);
					isChanged = true;
				}else {
					break;
				}
			}
			if(single.length() == 0)
				result.add(i);
		}
		
		return result;
	}

	//初始化map
	private static Map<String, Integer> initMap(String[] words) {
		Map<String, Integer> map = new HashMap<>();
		for(int i = 0; i < words.length; i++) {
			if(map.get(words[i]) == null)
				map.put(words[i], 1);
			else {
				map.put(words[i], map.get(words[i]) + 1);
			}
		}
		return map;
	}

}
