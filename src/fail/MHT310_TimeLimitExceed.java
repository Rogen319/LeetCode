package fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MHT310_TimeLimitExceed {
	
	public static void main(String[] args) {
		int n = 6;
		int[][] edges = {{0,1},{0,2},{0,3},{3,4},{4,5}};
		System.out.println(findMinHeightTrees(n, edges));
	}

	public static List<Integer> findMinHeightTrees(int n, int[][] edges) {
		List<Integer> res = new ArrayList<>();
		int[] heights = new int[n];
		
		//构图
		int[][] graph = new int[n][n];
		for(int i = 0; i < edges.length; i++) {
			int r = edges[i][0], c = edges[i][1];
			graph[r][c] = 1;
			graph[c][r] = 1;
		}
		
		boolean[] isVisited;
		List<Integer> heightList;
		//遍历每一个节点，找到树高
		for(int i = 0; i < n; i++) {
			isVisited = new boolean[n];
			heightList = new ArrayList<>();
			getHeightOfTree(i, graph, isVisited, heightList, 0);
			heights[i] = heightList.stream().min((i1,i2)-> i2 -i1).get();
		}
		int minHeight = Arrays.stream(heights).min().getAsInt();
		for(int i = 0; i < n; i++) {
			if(heights[i] == minHeight)
				res.add(i);
		}
		
		return res;
	}

	//找到以当前节点为根节点所存在的最长路径
	private static void getHeightOfTree(int i, int[][] graph, boolean[] isVisited, List<Integer> heights, int height) {
		isVisited[i] = true;
		height++;
		LinkedList<Integer> neighbours = getNeighbours(i, graph);
		
		boolean isLeaf = true;
		for(Integer index : neighbours) {
			if(!isVisited[index]) {
				isLeaf = false;
				getHeightOfTree(index, graph, isVisited, heights, height);
			}
		}
		
		if(isLeaf) {
			heights.add(height);
		}
		
	}
	
	private static LinkedList<Integer> getNeighbours(int i, int[][] graph) {
		LinkedList<Integer> res = new LinkedList<>();
		
		for(int j = 0; j < graph[0].length; j++) {
			if(graph[i][j] == 1)
				res.add(j);
		}
		
		return res;
	}

}
