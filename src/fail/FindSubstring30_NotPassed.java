package fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindSubstring30_NotPassed {

	public static void main(String[] args) {
		String[] words = {"bar","foo","the"};
		System.out.println(findSubstring("barfoofoobarthefoobarman", words));
	}

	public static List<Integer> findSubstring(String s, String[] words) {
		List<Integer> result = new ArrayList<>();
		if(words.length == 0)
			return result;
		//取出第一个words并计算长度
		String begin = words[0];
		int length = begin.length();
		int start = 0;
		
		String[] segment = s.split(begin);
		for(int i = 0; i < segment.length; i++) {
			//构造每个要进行判断的字符串
			String single = segment[i];
			if(start + segment[i].length() < s.length())
				single += begin;
			if(i + 1 < segment.length)
				single += segment[i + 1];
			
			//对每个字符串进行判断：查找index然后看是否连续
			/**
			 * 错误：除了words[0]，其它的word在single里可能出现多次，
			 * 而此方法只会取第一个位置的下标，从而导致出错
			 */
			System.out.print(single + "	");
			boolean isContinued = true;
			int[] indexes = new int[words.length];
			for(int j = 0; j < words.length; j++) {
				int index = single.indexOf(words[j]);
				if(index != -1)
					indexes[j] = index;
				else {
					isContinued = false;
					break;
				}
			}
			System.out.println(Arrays.toString(indexes));
			//判断是否连续
			if(isContinued) {
				Arrays.sort(indexes);
				for(int j = 1; j < indexes.length; j++) {
					if(indexes[j] - indexes[j - 1] == length) {
						if(j == indexes.length - 1)
							result.add(start + indexes[0]);
						continue;
					}
					else
						break;
				}
			}
			start += segment[i].length() + length;
		}
		
		return result;
	}

}
