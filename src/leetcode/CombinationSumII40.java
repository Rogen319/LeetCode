package leetcode;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class CombinationSumII40 {

	public static void main(String[] args) {
		int[] candidates = {10,1,2,7,6,1,5};
		int target = 8;
		System.out.println(combinationSum2(candidates, target));
	}

	public static List<List<Integer>> combinationSum2(int[] candidates, int target) {
		List<List<Integer>> ret = new ArrayList<>();
		if(candidates == null || candidates.length == 0)
			return ret;
		
		Arrays.sort(candidates);
//		System.out.println(Arrays.toString(candidates));
		
		List<Integer> single = new ArrayList<>();
		
		Set<List<Integer>> set = new HashSet<>();
		
		dfs(set, single, candidates, target, 0);
		ret = new ArrayList<>(set);
		return ret;
	}

	private static void dfs(Set<List<Integer>> set, List<Integer> single, int[] candidates, int target, int i) {
		if(target == 0) {
			/**
			 * 这里之所以不能够直接add(single)的原因在于，single是一个对象，它的值一直在变化，
			 * 最终为[]。所以要以满足条件的single为值，新建一个list对象并存储，这样后续才不会变为空
			 */
			set.add(new ArrayList<>(single));
//			System.out.println(single);
//			ret.add(single);
		}
		
		for(int j = i; j < candidates.length && target >= candidates[j]; j++) {
			single.add(candidates[j]);
//			System.out.println(single);
			dfs(set, single, candidates, target - candidates[j], j + 1);
			single.remove(single.size() - 1);
//			System.out.println(single);
		}
		
	}
	
	

}
