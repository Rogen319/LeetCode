package leetcode;

import java.util.Arrays;

public class SudokuSolver37 {

	public static void main(String[] args) {
		char[][] board = {
				{'5','3','.','.','7','.','.','.','.'},
				{'6','.','.','1','9','5','.','.','.'},
				{'.','9','8','.','.','.','.','6','.'},
				{'8','.','.','.','6','.','.','.','3'},
				{'4','.','.','8','.','3','.','.','1'},
				{'7','.','.','.','2','.','.','.','6'},
				{'.','6','.','.','.','.','2','8','.'},
				{'.','.','.','4','1','9','.','.','5'},
				{'.','.','.','.','8','.','.','7','9'}
				};
		solveSudoku(board);
		for(int i = 0; i < board.length; i++)
			System.out.println(Arrays.toString(board[i]));
	}

	public static void solveSudoku(char[][] board) {
		if(board == null || board.length == 0)
			return;
		solve(board, 0);
	}
	
	private static boolean solve(char[][] board, int position) {
		if(position == 81)
			return true;
		//比较巧妙的遍历方法：将二维的转变为一维进行遍历
		int row = position / 9;
		int col = position % 9;
		
		if(board[row][col] == '.') {
			for(char c = '1'; c <= '9'; c++) {
				if(isValid(board, c, row, col)) {
					board[row][col] = c;
					if(solve(board, position + 1)) {
						return true;
					}
					else {
						board[row][col] = '.';
					}
				}
			}
		}else {
			if(solve(board, position + 1))
				return true;
		}
		return false;
	}

	private static boolean isValid(char[][] board, char c, int row, int col) {
		//检查行
		for(int i = 0; i < 9; i++) {
			if(board[row][i] == c)
				return false;
		}
		
		//检查列
		for(int i = 0; i < 9; i++) {
			if(board[i][col] == c)
				return false;
		}
		
		//检查所在的子数独
		/**
		 * 无法处理row或者col刚好是3的倍数的情况，此时rowStart和rowEnd
		 * 按照这种方式计算出来的值是一样的！
		 */
//		int rowStart = row, rowEnd = row;
//		int colStart = col, colEnd= col;
//		//计算起止下标
//		while(rowStart % 3 != 0)
//			rowStart--;
//		while(rowEnd % 3 != 0)
//			rowEnd++;
//		while(colStart % 3 != 0)
//			colStart--;
//		while(colEnd % 3 != 0)
//			colEnd++;
		int rowStart = row / 3 * 3;
		int colStart = col / 3 * 3;
		for(int i = rowStart; i < rowStart + 3; i++) {
			for(int j = colStart; j < colStart + 3; j++) {
				if(board[i][j] == c)
					return false;
			}
		}
		
		return true;
	}

}
