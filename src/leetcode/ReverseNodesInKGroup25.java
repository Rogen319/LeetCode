package leetcode;

public class ReverseNodesInKGroup25 {

	public static void main(String[] args) {
		ListNode node1 = new ListNode(1);
		ListNode node2 = new ListNode(2);
		node1.next = node2;
		ListNode node = reverseKGroup(node1, 2);
		while(node != null) {
			System.out.println(node.val);
			node = node.next;
		}
	}
	
	public static ListNode reverseKGroup(ListNode head, int k) {
		ListNode dummy = new ListNode(0);
		dummy.next = head;
		ListNode currentHead = dummy, currentTail = dummy;
		int count = 0;
		while(currentTail != null) {
			if(count < k) {
				count++;
				currentTail = currentTail.next;
			}
			else{
//				System.out.println(currentHead.val + "    " + currentTail.val);
				//调整首尾的指针
				ListNode first = currentHead.next;
                ListNode nextStart = currentTail.next;
				currentHead.next = currentTail;
				//反转指针
				ListNode current = first;
				ListNode next = current.next;
				while(count > 1) {
					ListNode temp = next.next;
					next.next = current;
					current = next;
					next = temp;
					count--;
				}
				//再次开始计数
				first.next = nextStart;
				count = 0;
				currentHead = first;
				currentTail = first;
			}
		}
		return dummy.next;
	}
	
}

class ListNode {
	int val;
	ListNode next;
	ListNode(int x) { 
		val = x; 
	}
}