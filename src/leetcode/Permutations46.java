package leetcode;

import java.util.ArrayList;
import java.util.List;

public class Permutations46 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public List<List<Integer>> permute(int[] nums) {
		List<List<Integer>> res = new ArrayList<>();
		
		int size = nums.length;
		boolean[] visited = new boolean[size];
		
		List<Integer> single = new ArrayList<>();
		
		dfs(res,single,0,visited,nums);
		
		return res;
	}

	private void dfs(List<List<Integer>> res, List<Integer> single, int level, boolean[] visited, int[] nums) {
		if(level == nums.length) {
			res.add(new ArrayList<>(single));
			return;
		}
		for(int i = 0; i < nums.length; i++) {
			if(visited[i] == false) {
				visited[i] = true;
				single.add(nums[i]);
				dfs(res, single, level + 1, visited, nums);
				single.remove(single.size() - 1);
				visited[i] = false;
			}
		}
	}

}
