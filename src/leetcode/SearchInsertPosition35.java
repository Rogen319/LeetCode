package leetcode;

public class SearchInsertPosition35 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public int searchInsert(int[] nums, int target) {
		for(int i = 0; i < nums.length; i++) {
			if(nums[i] >= target)
				return i;
		}
		return nums.length;
	}

}
