package leetcode;

public class EditDistance72 {

	public static void main(String[] args) {

	}

	public int minDistance(String word1, String word2) {
		if(word1 == null || word2 == null)
			return 0;
		int n = word1.length();
		int m = word2.length();
		
		int[][] dp = new int[n + 1][m + 1];
		for(int i = 0; i <= n; i++) 
			dp[i][0] = i;
		for(int i = 0; i <= m; i++)
			dp[0][i] = i;
		
		for(int i = 1; i <= n; i++) {
			for(int j = 1; j <= m; j++) {
				if(word2.charAt(j - 1) == word1.charAt(i - 1))
					dp[i][j] = dp[i - 1][j - 1];
				else {
					int min = Math.min(dp[i - 1][j], dp[i][j - 1]);
					min = Math.min(dp[i - 1][j - 1], min);
					dp[i][j] = min + 1;
				}
			}
		}
		
		return dp[n][m];
	}

}
