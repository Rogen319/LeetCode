package leetcode;

import java.util.Arrays;

/*
 * 超过100%！！！
 */
public class SearchRange34 {

	public static void main(String[] args) {
//		int[] nums = {5,7,7,8,8,10};
		int[] nums = {1,4};
		System.out.println(Arrays.toString(searchRange(nums, 4)));
	}

	public static int[] searchRange(int[] nums, int target) {
		int[] result =new int[2];
		result[0] = -1;
		result[1] = -1;
		
		if(nums == null || nums.length == 0)
			return result;
		
		int begin = 0;
		int end = nums.length - 1;
		int middle = (begin + end) / 2;
		//二分查找
		/*
		 * 精髓：end >= begin这个循环终止条件！
		 */
		while(nums[middle] != target && end >= begin) {
			if(nums[middle] < target) {
				begin = middle + 1;
				middle = (begin + end) / 2;
			}else {
				end = middle - 1;
				middle = (begin + end) / 2;
			}
//			System.out.println(String.format("begin is %d, end is %d, middle is %d", begin, end, middle));
		}
		if(nums[middle] == target) {
			int from = middle;
			int to = middle;
			//处理多个值的情况
			while(from > 0 && nums[from] == target) 
				from--;
			while(to < nums.length - 1 && nums[to] == target) 
				to++;
			//处理多减的情况
			if(from >= 0 && nums[from] != target)
				from = from + 1;
			//处理多加的情况
			if(to < nums.length && nums[to] != target)
				to = to - 1;
			result[0] = from;
			result[1] = to;
		}
		
		return result;
	}

	
}
