package leetcode;

import java.util.HashMap;
import java.util.Map;

public class ValidSudoku36 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static boolean isValidSudoku(char[][] board) {
		boolean ret = true;

		Map<Character, Integer> map = new HashMap<>();

		//判断9个子区域是否valid
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				map.clear();
				for(int m = i * 3; m < i * 3 + 3; m++) {
					for(int n = j * 3; n < j * 3 + 3; n++) {
						char key = board[m][n];
						if(key != '.') {
							if(map.get(key) == null) {
								map.put(key, 1);
							}else {
								return false;
							}
						}
					}
				}
			}
		}

		//判断每一行是否valid
		for(int i = 0; i < 9; i++) {
			map.clear();
			for(int j = 0; j < 9; j++) {
				char key = board[i][j];
				if(key != '.') {
					if(map.get(key) == null) {
						map.put(key, 1);
					}else {
						return false;
					}
				}
			}
		}

		//判断每一列是否valid
		for(int i = 0; i < 9; i++) {
			map.clear();
			for(int j = 0; j < 9; j++) {
				char key = board[j][i];
				if(key != '.') {
					if(map.get(key) == null) {
						map.put(key, 1);
					}else {
						return false;
					}
				}
			}
		}

		return ret;

	}

}
