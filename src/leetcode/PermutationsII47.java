package leetcode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PermutationsII47 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public List<List<Integer>> permuteUnique(int[] nums) {
		Set<List<Integer>> res = new HashSet<>();
		if(nums == null || nums.length == 0)
			return null;
		List<Integer> single = new ArrayList<>();
		boolean[] visited = new boolean[nums.length];
		
		dfs(res, single, 0, nums, visited);
		
		return new ArrayList<>(res);
	}

	private void dfs(Set<List<Integer>> res, List<Integer> single, int level, int[] nums, boolean[] visited) {
		if(level == nums.length)
			res.add(new ArrayList<>(single));
		
		for(int i = 0; i < nums.length; i++) {
			if(visited[i] == false) {
				visited[i] = true;
				single.add(nums[i]);
				dfs(res, single, level + 1, nums, visited);
				single.remove(single.size() - 1);
				visited[i] = false;
			}
		}
	}

}
