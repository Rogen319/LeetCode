package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//�����ϵ�
public class CombinationSum39 {

	public static void main(String[] args) {
		int[] candidates = {2,3,6,7};
		int target = 7;
		List<List<Integer>> ret = combinationSum(candidates, target);
		System.out.println(ret.toString());
	}

	public static List<List<Integer>> combinationSum(int[] candidates, int target) {
		Arrays.sort(candidates);

		List<List<Integer>> ret = new ArrayList<>();
		List<Integer> temp = new ArrayList<>();
		
		if(candidates == null || candidates.length == 0)
			return ret;
		dfs(ret, temp, target, candidates, 0);
		
		return ret;
	}
	
	private static void dfs(List<List<Integer>> ret, List<Integer> temp, int target, int[] candidates, int j) {
		if(target == 0) {
			ret.add(new ArrayList<>(temp));
		}
		for(int i = j; i < candidates.length && target >= candidates[i]; i++) {
			temp.add(candidates[i]);
			dfs(ret, temp, target - candidates[i], candidates, i);
			temp.remove(temp.size() - 1);
		}
	}

}
