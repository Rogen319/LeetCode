package leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindSubstring30 {

	public static void main(String[] args) {
		String[] words = {"word","good","best","good"};
		System.out.println(findSubstring("wordgoodgoodgoodbestword", words));
	}

	public static List<Integer> findSubstring(String s, String[] words) {
		List<Integer> result = new ArrayList<>();
		if(words.length == 0)
			return result;
		//记录words里面的每个word以及出现的次数
		Map<String, Integer> map;
		//计算单个Word的长度
		int length = words[0].length();
		
		//初始化map
		map = initMap(words);
		
		for(int i = 0; i < length; i++) {
			Map<String, Integer> curMap = new HashMap<>();
			int count = 0;
			int left = i;
			for(int j = i; j <= s.length() - length; j += length) {
				String str = s.substring(j, j + length);
				if(map.containsKey(str)) {
					if(curMap.containsKey(str)) {
						curMap.put(str, curMap.get(str) + 1);
					}else {
						curMap.put(str, 1);
					}
					if(curMap.get(str) <= map.get(str)) {
						count++;
					}else {
						while(curMap.get(str) > map.get(str)) {
							String temp = s.substring(left,left + length);
							curMap.put(temp, curMap.get(temp) - 1);
							if(curMap.get(temp) < map.get(temp))
								count--;
							left += length;
						}
					}
					if(count == words.length) {
						result.add(left);
						String temp = s.substring(left,left + length);
						curMap.put(temp, curMap.get(temp) - 1);
						count--;
						left += length;
					}
				}else {
					count = 0;
					curMap.clear();
					left = (j + length);
				}
			}
		}
		
		return result;
	}

	//初始化map
	private static Map<String, Integer> initMap(String[] words) {
		Map<String, Integer> map = new HashMap<>();
		for(int i = 0; i < words.length; i++) {
			if(map.get(words[i]) == null)
				map.put(words[i], 1);
			else {
				map.put(words[i], map.get(words[i]) + 1);
			}
		}
		return map;
	}

}
