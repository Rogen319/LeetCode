package leetcode;

import java.util.LinkedList;

public class LongestValidParentheses32 {

	public static void main(String[] args) {
		System.out.println(longestValidParentheses("(())"));
	}

	//来自网上
	public static int longestValidParentheses(String s) {
		if(s.length() < 2)
			return 0;
		//去除左边多余的)
		int begin = s.indexOf("(");
		if(begin != -1)
			s = s.substring(begin);

		LinkedList<Integer> stack = new LinkedList<>();

		int start = -1;
		int maxLength = 0;
		for(int i = 0; i < s.length(); i++) {
			if(s.charAt(i) == '(') {
				stack.push(i);
			}else {
				if(!stack.isEmpty()) {
					stack.pop();
					if(stack.isEmpty()) {
						maxLength = Math.max(i - start, maxLength);
					}else {
						/*
						 * 这里之所以是在pop之后用peek，是因为这种情况下peek的下标其实就是pop减一，所以整体效果
						 * 相当于i - stack.pop + 1。很巧妙啊！！！
						 */
						maxLength = Math.max(i - stack.peek(), maxLength);
					}
				}else {
					start = i;
				}
			}
		}
		return maxLength;
	}
}
