package leetcode;

public class UniquePathsII63 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public int uniquePathsWithObstacles(int[][] obstacleGrid) {
		if(obstacleGrid == null || obstacleGrid.length == 0 || obstacleGrid[0][0] == 1)
			return 0;
		int m = obstacleGrid.length;
		int n = obstacleGrid[0].length;
		
		int[][] dp = new int[m][n];
		dp[0][0] = 1;
		
		for(int i = 0; i < m; i ++) {
			for(int j = 0; j < n; j++) {
				if(obstacleGrid[i][j] == 0) {
					if((i - 1) >= 0 && (j - 1) >= 0 && obstacleGrid[i - 1][j] == 0 && obstacleGrid[i][j - 1] == 0)
						dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
					else if((i - 1) >= 0 && obstacleGrid[i - 1][j] == 0)
						dp[i][j] = dp[i - 1][j];
					else if((j - 1) >= 0 && obstacleGrid[i][j - 1] == 0)
						dp[i][j] = dp[i][j - 1];
				}
			}
		}
		
		return dp[m - 1][n - 1];
	}

}
