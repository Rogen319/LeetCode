package leetcode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class MHT310 {

	public static void main(String[] args) {
		int n = 6;
		int[][] edges = {{0,1},{0,2},{0,3},{3,4},{4,5}};
		System.out.println(findMinHeightTrees(n, edges));
	}

	public static List<Integer> findMinHeightTrees(int n, int[][] edges) {
		List<Integer> leaves = new ArrayList<>();
		
		if(edges == null || edges.length == 0) {
			leaves.add(0);
			return leaves;
		}
		
		List<Set<Integer>> adj = new ArrayList<>(n);
		for(int i = 0; i < n; i++)
			adj.add(new HashSet<>());

		//构图
		for(int i = 0; i < edges.length; i++) {
			int r = edges[i][0], c = edges[i][1];
			adj.get(r).add(c);
			adj.get(c).add(r);
		}

		//叶子节点
		for(int i = 0; i < n; i++) {
			if(adj.get(i).size() == 1)
				leaves.add(i);
		}
		
		//循环结束条件，个数小于等于2
		List<Integer> newLeaves;
		while(n > 2) {
			n -= leaves.size();
			
			newLeaves = new ArrayList<>();
			for(int i = 0; i < leaves.size(); i++) {
				int index = adj.get(leaves.get(i)).iterator().next();
				adj.get(index).remove(leaves.get(i));
				if(adj.get(index).size() == 1)
					newLeaves.add(index);
			}
			
			leaves = newLeaves;
		}

		return leaves;
	}

	//找到以当前节点为根节点所存在的最长路径
	private static void getHeightOfTree(int i, int[][] graph, boolean[] isVisited, List<Integer> heights, int height) {
		isVisited[i] = true;
		height++;
		LinkedList<Integer> neighbours = getUnvisitedNeighbours(i, graph, isVisited);

		if(neighbours.size() == 0) {
			heights.add(height);
		}else {
			for(Integer index : neighbours) {
				getHeightOfTree(index, graph, isVisited, heights, height);
			}
		}

	}

	private static LinkedList<Integer> getUnvisitedNeighbours(int i, int[][] graph, boolean[] isVistied) {
		LinkedList<Integer> res = new LinkedList<>();

		for(int j = 0; j < graph[0].length; j++) {
			if(graph[i][j] == 1 && !isVistied[j])
				res.add(j);
		}

		return res;
	}

}
