package leetcode;

public class PartitionEqualSubsetSum416 {
	
	public static void main(String[] args) {
		int[] nums = {1,2,5};
		System.out.println(canPartition(nums));
	}

	public static boolean canPartition(int[] nums) {
		if(nums == null || nums.length < 2)
			return false;

		int sum = 0;
		for(int i = 0; i < nums.length; i++) {
			sum += nums[i];
		}

		if(sum % 2 != 0)
			return false;

		int target = sum / 2;
		
		boolean[] flag = new boolean[target + 1];
		flag[0] = true;
		for(int i = 0; i < nums.length; i++) {
			//注意这里要从target遍历到nums[i]，否则{1,2,5}的情况就会返回true，实际为false
			//因为遇到1的时候，会导致1到5全为true
			for(int j = target; j >= nums[i]; j--) {
//				System.out.println(nums[i]);
				if(flag[j - nums[i]] == true)
					flag[j] = true;
			}
		}
		
		return flag[target];
	}

}
