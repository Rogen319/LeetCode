package leetcode;

import java.util.Arrays;


public class NextPermutation31 {

	public static void main(String[] args) {
		int[] nums = {2,3,1};
		nextPermutation(nums);
		System.out.println(Arrays.toString(nums));
	}

	public static void nextPermutation(int[] nums) {
		boolean isExist = false;
		for(int i = nums.length - 1; i > 0; i--) {
			//找到第一个后面比前面大的下标
			if(nums[i] > nums[i - 1]) {
				//找到i以及i之后最接近nums[i - 1]的值，并与nums[i - 1]交换
				int index = findNearest(i-1, nums);
				System.out.println(i + " " + index);
				swap(i - 1, index, nums);
				//将i及以后的元素按升序排序
				sort(i, nums);
				isExist = true;
				break;
			}
		}
		//不存在的话，排序即可
		if(!isExist)
			Arrays.sort(nums);
	}

	private static void sort(int i, int[] nums) {
		System.out.println(i);
		if(i == nums.length - 1)
			return;
		for(int j = i + 1; j < nums.length; j++) {
			int temp = nums[j];
			int k;
			for( k = j - 1; k >= i; k--) {
				System.out.println(temp + "  " + nums[k]);
				if(temp < nums[k]) {
					nums[k + 1] = nums[k];
				}
				else
					break;
				System.out.println(Arrays.toString(nums));
			}
			k++;
			System.out.println(k);
			nums[k] = temp;
		}
	}

	//交换元素顺序
	private static void swap(int i, int index, int[] nums) {
		int temp = nums[i];
		nums[i] = nums[index];
		nums[index] = temp;		
	}

	//找到i以及i之后最接近nums[i - 1]的值
	private static int findNearest(int i, int[] nums) {
		int result = -1;
		int v = nums[i];
		int diff = Integer.MAX_VALUE;
		for(int j = i + 1; j < nums.length; j++) {
			if(nums[j] - v > 0 && nums[j] - v < diff) {
				diff = nums[j] - v;
				result = j;
			}
		}
		return result;
	}

}
