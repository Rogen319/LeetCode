package leetcode;

public class SearchinRotatedSortedArray33 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public int search(int[] nums, int target) {
		if(nums.length == 0)
			return -1;
		
		int pivot = nums[nums.length - 1];
		if(target < pivot) {
			int previous = pivot;
			int index = nums.length - 2;
			while(index >= 0 && nums[index] < previous) {
				if(nums[index] == target)
					return index;
				index--;
			}
		}else if(target > pivot) {
			int index = 0;
			while(index < nums.length) {
				if(nums[index] == target)
					return index;
				if(index + 1 < nums.length && nums[index] > nums[index + 1])
					return -1;
				index++;
			}
		}else {
			return nums.length - 1;
		}
		return -1;
	}

}
