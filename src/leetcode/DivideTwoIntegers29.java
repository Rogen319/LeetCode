package leetcode;

public class DivideTwoIntegers29 {

	public static void main(String[] args) {
		System.out.println(divide(2147483647, 3));
	}

	public static int divide(int dividend, int divisor) {
		if(divisor == 0)
			return 0;

		boolean isNeg = (dividend < 0 && divisor > 0) || (dividend > 0 && divisor < 0);

		long dividend_long = Math.abs((long)(dividend));
		long divisor_long = Math.abs((long)(divisor));

		if(dividend_long < divisor_long)
			return 0;

		long answer = 0;
		long temp = 1;
		long exp = divisor_long;

		while((dividend_long - exp) >= 0) {
			answer += temp;
			temp = temp << 1;
			dividend_long = dividend_long - exp;
			exp = exp << 1;
		}


		while(dividend_long != 0 && (dividend_long - exp) < 0) {
			exp = exp >> 1;
			temp = temp >> 1;
		}

		//错误：里面还需要一个循环，对exp进行递归的减少，每次都要找到最小的合适的
		//		while((dividend_long - exp) >= 0 && temp >= 1) {
		//			answer += temp;
		//			temp = temp >> 1;
		//			dividend_long = dividend_long - exp;
		//			exp = exp >> 1;
		//		}

		while((dividend_long - exp) >= 0 && temp >= 1) {
			answer += temp;
			dividend_long = dividend_long - exp;
			
			//需要这一个循环，每次找到合适的exp值
			while(temp >= 1 && (dividend_long - exp) < 0) {
				temp = temp >> 1;
				exp = exp >> 1;
			}
		}

		if(isNeg)
			answer = -answer;

		if(answer > Integer.MAX_VALUE)
			return Integer.MAX_VALUE;

		return (int)answer;
	}

}
