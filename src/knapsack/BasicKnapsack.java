package knapsack;

import java.util.Scanner;

public class BasicKnapsack {

	public static void main(String[] args) {
		//读取系统输入
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		//物品的数量以及背包的容量
		int n = scanner.nextInt();
		int c = scanner.nextInt();

		//读取物品的重量及价值
		int[] w = new int[n];
		int[] v = new int[n];
		for(int i = 0; i < n; i++) {
			w[i] = scanner.nextInt();
		}
		for(int i = 0; i < n; i++) {
			v[i] = scanner.nextInt();
		}

		//Basic 0-1 knapsack problem
		int[][] f = new int[n + 1][c + 1];
		for(int i = 0; i <= n; i++)
			f[i][0] = 0;
		for(int i = 0; i <= c; i++)
			f[0][i] = 0;

		basicKnapsack(n,c,w,v,f);
		for (int i = 0, j = 0; i < f.length;) {
			System.out.print(f[i][j] + "\t");
			j++;
			if (j >= f[i].length) {
				i++;
				j = 0;
				System.out.println();
			}
		}
	}

	private static void basicKnapsack(int n, int c, int[] w, int[] v, int[][] f) {
		for(int i = 1; i <= n; i++) {
			for(int j = 1; j <= c; j++) {
				if(j < w[i - 1])
					f[i][j] = f[i - 1][j];
				else {
					f[i][j] = Math.max(f[i - 1][j], f[i - 1][j - w[i - 1]] + v[i - 1]);
				}
			}
		}
	}

}
